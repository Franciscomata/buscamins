/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Formulario.Principal;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import static jdk.nashorn.internal.objects.NativeDebug.getClass;

/**
 *
 * @author paco
 */
public class Celda extends JButton{
    private int x;
    private int y;
    private int tipo;
    private boolean visible;
    private Color color[];

    public boolean getVisible() {
        return visible;
    }

    @Override
    public void setVisible(boolean visible) {
        this.visible = visible;
    }
    
    public Celda(int x, int y) {
        this.x = x;
        this.y = y;
        this.visible=false;
        this.color=new Color[]{Color.BLUE,Color.GREEN,Color.CYAN,Color.RED,Color.DARK_GRAY,Color.ORANGE};
        this.setMinimumSize(new Dimension(35,10));
        this.setBackground(new java.awt.Color(0,0,204));
        this.setFont(new java.awt.Font("Tahoma",1,12));
        
        this.addActionListener(new java.awt.event.ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                celdaActionPerformed(evt);
            }
        });
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
     private void celdaActionPerformed(ActionEvent evt) {
          clic();
          int count=0;
          for (int i = 0; i < Principal.filas; i++) {
              for (int j = 0; j < Principal.columnas; j++) {
                  if(Principal.celda[i][j].getVisible()){
                     count++;
                  }
              }
         }
          if(count==(Principal.filas*Principal.columnas-Principal.minas)){
              Principal.fin=true;
              
              //aqui poner salida cuando gana felicitacion o algo
          }
     }
     
     public void clic(){
         if(!visible && Principal.fin==false){
             this.visible=true;
             this.setBackground(new java.awt.Color(240,240,240));
             switch(this.tipo){
                 case 0:
                     for (int i = 0; i < Principal.filas; i++) {
                         for (int j = 0; j < Principal.columnas; j++) {
                             if(Principal.celda[i][j].getTipo()==0){
                                  Principal.celda[i][j].setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bomba.png")));   
                             }  
                         }
                     }
                     Principal.fin=true;
                     break;
                 case 1:
                     int cont=0;
                     this.setBackground(new java.awt.Color(240,240,240,240));
                     for (int i = -1; i <= 1; i++) {
                         if(x+i>=0&&x+i<Principal.filas){
                             for (int j = -1; j <= 1; j++) {
                                 if((y+j>=0&&(y+j)<Principal.columnas)&&Principal.celda[x+i][y+j].getTipo()==0){
                                     cont++;
                                 }
                             }
                         }
                     }
                     this.setText(""+cont);
                     this.setForeground(this.color[cont]);
                     break;
                 default:
                     for (int i = -1; i <= 1; i++) {
                        if(x+i>=0&&x+i<Principal.filas){
                             for (int j = -1; j <= 1; j++) {
                                 if((y+j>=0&&(y+j)<Principal.columnas)&&Principal.celda[x+i][y+j].getTipo()!=0){
                                   if(!Principal.celda[x+i][y+j].getVisible()) {
                                       Principal.celda[x+i][y+j].clic();
                                   } 
                                 }
                             }
                        }
                     }
             }
         }   
     }

    public int getTipo() {
        return tipo;
    }
    
}
